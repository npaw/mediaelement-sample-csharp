﻿using System.Reflection;
using Windows.UI.Xaml.Controls;
using Youbora.MediaElementAdapter;

namespace Ocs.Models
{
    public class Class1 : MediaElementAdapter
    {
        public override void Player_MediaFailed(object sender, Windows.UI.Xaml.ExceptionRoutedEventArgs e)
        {
            //envoie de l'erreur à youbora
            var errorTab = e.ErrorMessage.Split(':');
            string codeError = errorTab[0];

            FireError(codeError, "abcd");
            FireStop();
        }
    }
}
