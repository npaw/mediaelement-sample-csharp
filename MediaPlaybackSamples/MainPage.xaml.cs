﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.System.Threading;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Youbora;
using Youbora.MediaElementAdapter;
using Newtonsoft.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MediaPlaybackSamples
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Youbora.plugin.Plugin PluginInstance = null;

        public MainPage()
        {
            this.InitializeComponent();

            // Youbora
            Youbora.log.Log.DebugLevel = Youbora.log.Log.Level.VERBOSE; // Optional debug level
            PluginInstance = new Youbora.plugin.Plugin(new Dictionary<string, object>()
            {
                ["accountCode"] = "powerdev",
                ["content.title"] = "a title"
            });
            Application.Current.EnteredBackground += PluginInstance.backgroundDetector.ToBackground;
            Application.Current.LeavingBackground += PluginInstance.backgroundDetector.ToForeground;
            PluginInstance.SetAdapter(new MediaElementAdapter(mePlayer));
            //PluginInstance.infinity.Begin();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            var mediaSource = MediaSource.CreateFromUri(new Uri(txtInputURL.Text));
            var mediaPlaybackItem = new MediaPlaybackItem(mediaSource);
            mePlayer.SetPlaybackSource(mediaPlaybackItem);
        }
    }
}
